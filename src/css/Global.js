import { createGlobalStyle } from 'styled-components';

import Reset from './Reset';
import Style from './Style';

const GlobalStyle = createGlobalStyle`
	${Reset};
	${Style};
`;

export default GlobalStyle;
