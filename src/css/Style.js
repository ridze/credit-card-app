import { css } from 'styled-components';

const Style = css`
	* {
		font-size: 16px;
		font-family: "Roboto Light";
		color: rgba(0, 0, 0, 0.87);
	}
	/* Chrome, Safari, Edge, Opera */
	input::-webkit-outer-spin-button,
	input::-webkit-inner-spin-button {
		-webkit-appearance: none;
		margin: 0;
	}
	
	/* Firefox */
	input[type=number] {
		-moz-appearance:textfield;
	}
`;

export default Style;
