import { LOCAL_STORAGE_PREFIX_KEY } from './constants/Generic';

function getKey(key) {
	return `${LOCAL_STORAGE_PREFIX_KEY}${key}`;
}

export default {
	setItem: function (key, value) {
		localStorage.setItem(getKey(key), JSON.stringify(value));
	},

	getItem: function (key) {
		return JSON.parse(localStorage.getItem(getKey(key)));
	},
};
