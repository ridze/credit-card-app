import { css } from 'styled-components';

export const FontSizes = {
	base: css`
		font-size: 16px;	
	`,
	big: css`
		font-size: 20px;
	`,
	huge: css`
		font-size: 24px;
	`,
	pageHeading: css`
		font-size: 35px;
	`,
};

export const Colors = {
	base: css`
		color: rgba(0, 0, 0, 0.87);
	`,
	baseWhite: css`
		color: rgb(255, 255, 255);
	`,
	baseRed: css`
		color: rgb(220, 53, 69);
	`,
};

export const BackgroundColors = {
	baseBlue: css`
		background: rgb(0, 105, 217);
	`,
	transparentBlue: css`
		background: rgba(0, 105, 217, 0.5);
	`,
};
