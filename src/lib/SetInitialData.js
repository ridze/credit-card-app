// Lib
import LocalStorage from './LocalStorage';

// Constants
import { CARDS_INITIAL_DATA } from './constants/Generic';

export default (() => {
	try {
		if (!LocalStorage.getItem('cards')) {
			LocalStorage.setItem('cards', CARDS_INITIAL_DATA);
		}
		return true;
	} catch (error) {
		return error;
	}
}) (); // eslint-disable-line
