import React from 'react';

// Atoms
import { CardWrapper, PlusSign } from '../atoms/Card';

const AddNewCard = () => (
	<CardWrapper>
		<PlusSign>+</PlusSign>
	</CardWrapper>
);

export default AddNewCard;
