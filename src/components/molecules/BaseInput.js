import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

// Atoms
import { InputComponent } from '../atoms/Input';
import { ErrorText } from '../atoms/Text';

// Molecules
import BaseLabel from './BaseLabel';

const BaseInput = (props) => {
	const {
		type,
		onChange,
		label,
		error,
		defaultValue,
		removeLabel,
	} = props;

	return (
		<Fragment>
			{!removeLabel && <BaseLabel>{label}</BaseLabel>}
			<InputComponent
				type={type}
				onChange={onChange}
				defaultValue={defaultValue}
			/>
			<ErrorText>{error}</ErrorText>
		</Fragment>
	);
};

BaseInput.propTypes = {
	type: PropTypes.string.isRequired,
	error: PropTypes.string,
	label: PropTypes.string.isRequired,
	onChange: PropTypes.func.isRequired,
	defaultValue: PropTypes.string,
	removeLabel: PropTypes.bool,
};

BaseInput.defaultProps = {
	error: '',
	defaultValue: '',
	removeLabel: false,
};

export default BaseInput;
