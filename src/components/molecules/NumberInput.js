import React from 'react';
import PropTypes from 'prop-types';

// Atoms
import { InputComponent } from '../atoms/Input';

const NumberInput = (props) => {
	const {
		onChange,
		value,
		disabled,
		min,
		max,
		step,
		setRef,
	} = props;

	return (
		<InputComponent
			type="number"
			name="base-number-input"
			onChange={onChange}
			value={value}
			disabled={disabled}
			min={min}
			max={max}
			step={step}
			ref={setRef}
		/>
	);
};

NumberInput.propTypes = {
	onChange: PropTypes.func.isRequired,
	value: PropTypes.string,
	disabled: PropTypes.bool,
	min: PropTypes.string,
	max: PropTypes.string,
	step: PropTypes.string,
	setRef: PropTypes.func,
};

NumberInput.defaultProps = {
	value: '',
	disabled: false,
	min: '0',
	max: '9999',
	step: '1',
	setRef: () => {},
};

export default NumberInput;
