import React from 'react';
import PropTypes from 'prop-types';

import { LabelComponent, LabelWrapper } from '../atoms/Label';

const BaseLabel = ({ children }) => (
	<LabelWrapper>
		<LabelComponent>{children}</LabelComponent>
	</LabelWrapper>
);

BaseLabel.propTypes = {
	children: PropTypes.node.isRequired,
};

export default BaseLabel;
