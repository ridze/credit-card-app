import React from 'react';
import PropTypes from 'prop-types';

// Constants
import { CARD_TYPES, CARD_TYPES_BY_DIGITS } from '../../lib/constants/Generic';

// Images
import MastercardImage from '../../assets/images/card-type-mastercard.png';
import DiscoverImage from '../../assets/images/card-type-discover.png';
import VisaImage from '../../assets/images/card-type-visa.png';

// Atoms
import { FlexRowSpaceBetween } from '../atoms/Flex';

import {
	CardWrapper,
	CardChip,
	CardTypeWrapper,
	CardImageDiscover,
	CardImageMaster,
	CardImageVisa,
	CardNumber,
	UserName,
	ExpiresOn,
} from '../atoms/Card';

// Moment js
const moment = require('moment');

const Card = (props) => {
	const {
		cardNumber,
		userName,
		expiresOn,
	} = props;

	const getCardType = () => {
		if (cardNumber) {
			const [firstDigit] = cardNumber;
			return CARD_TYPES_BY_DIGITS[firstDigit];
		}
		return null;
	};

	const getImageByCardType = () => {
		const cardType = getCardType();
		if (!cardType) {
			return null;
		}

		switch (cardType) {
		case CARD_TYPES.DISCOVER: {
			return <CardImageDiscover src={DiscoverImage} />;
		}
		case CARD_TYPES.VISA: {
			return <CardImageVisa src={VisaImage} />;
		}
		case CARD_TYPES.MASTERCARD: {
			return <CardImageMaster src={MastercardImage} />;
		}
		default: {
			return null;
		}
		}
	};

	return (
		<CardWrapper>
			<CardTypeWrapper>
				{getImageByCardType()}
			</CardTypeWrapper>
			<CardChip />
			<CardNumber>{cardNumber}</CardNumber>
			<FlexRowSpaceBetween>
				<UserName>{userName}</UserName>
				<ExpiresOn>{expiresOn ? moment(expiresOn).format('MM/YY') : ''}</ExpiresOn>
			</FlexRowSpaceBetween>
		</CardWrapper>
	);
};

Card.propTypes = {
	cardNumber: PropTypes.string,
	userName: PropTypes.string,
	expiresOn: PropTypes.string,
};

Card.defaultProps = {
	cardNumber: null,
	userName: null,
	expiresOn: null,
};

export default Card;
