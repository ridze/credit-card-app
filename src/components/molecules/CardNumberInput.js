import React, { Fragment, Component } from 'react';
import PropTypes from 'prop-types';

// Atoms
import { FlexRowSpaceBetween } from '../atoms/Flex';
import { PaddingWrapper } from '../atoms/Wrapper';
import { ErrorText } from '../atoms/Text';

// Molecules
import NumberInput from './NumberInput';
import BaseLabel from './BaseLabel';

class CardNumberInput extends Component {
	constructor(props) {
		super(props);
		const { defaultValue } = props;

		const inputValues = [];
		this.inputKeys = [];
		this.inputRefs = [];

		let i = 0;
		while (i <= 3) {
			inputValues.push(defaultValue.slice(i * 4, i * 4 + 4));
			this.inputKeys.push(`card-input-${i}`);
			this.inputRefs.push(null);
			i += 1;
		}

		this.state = {
			inputValues,
		};
	}

	handleInputChange = (event, inputIndex) => {
		event.preventDefault();
		const { value } = event.target;
		const { onChange } = this.props;

		if (Number(value) >= 0 && value.length <= 4) {
			const { inputValues } = this.state;
			inputValues[inputIndex] = value;
			this.setState({ inputValues });
			if (value.length === 4 && inputIndex < 3) {
				this.inputRefs[inputIndex + 1].focus();
			}
			onChange(inputValues.join(''));
		}
	}
	;

	render() {
		const {
			inputValues,
		} = this.state;

		const {
			error,
		} = this.props;

		return (
			<Fragment>
				<BaseLabel>Card Number</BaseLabel>
				<FlexRowSpaceBetween>
					{inputValues.map((value, index) => (
						<PaddingWrapper widthPercentage={22} key={this.inputKeys[index]}>
							<NumberInput
								value={value}
								onChange={event => this.handleInputChange(event, index)}
								setRef={(node) => {
									this.inputRefs[index] = node;
								}}
							/>
						</PaddingWrapper>
					))}
				</FlexRowSpaceBetween>
				<ErrorText>{error}</ErrorText>
			</Fragment>
		);
	}
}

CardNumberInput.propTypes = {
	defaultValue: PropTypes.string,
	cardNumber: PropTypes.string,
	error: PropTypes.string,
	onChange: PropTypes.func.isRequired,
};

CardNumberInput.defaultProps = {
	defaultValue: '',
	cardNumber: '',
	error: '',
};

export default CardNumberInput;
