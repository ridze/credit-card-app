import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

// Constants
import { ALLOWED_CARD_INITIAL_DIGITS } from '../../lib/constants/Generic';

// Atoms
import { CardFormWrapper, PaddingWrapper } from '../atoms/Wrapper';
import { BaseButton } from '../atoms/Button';
import { BaseDivider } from '../atoms/Divider';

// Molecules
import BaseInput from '../molecules/BaseInput';
import Card from '../molecules/Card';
import CardNumberInput from '../molecules/CardNumberInput';

// Moment js
const moment = require('moment');

class CardForm extends PureComponent {
	constructor(props) {
		super(props);

		const { FOUR, FIVE, SIX } = ALLOWED_CARD_INITIAL_DIGITS;
		this.cardNumberErrorText = `Card number must begin with ${FOUR}, ${FIVE} or ${SIX}.`;
		this.expiresOnErrorText = 'Please choose a valid time point in future.';

		const { initialState } = props;

		const {
			id,
			cardNumber,
			userName,
			expiresOn,
		} = initialState;

		this.state = {
			id,
			cardNumber,
			userName,
			expiresOn,
			cardNumberError: this.getCardNumberError(cardNumber),
			expiresOnError: this.getExpiresOnError(expiresOn),
		};
	}

	isFormValid = (userName, cardNumber, expiresOn, cardNumberError, expiresOnError) => {
		return (!cardNumberError && !expiresOnError && cardNumber && cardNumber.length === 16 && userName && expiresOn);
	};

	getCardNumberError = (cardNumber) => {
		let isValid = false;
		const allowedInitialDigits = Object.values(ALLOWED_CARD_INITIAL_DIGITS);

		if (!cardNumber) {
			return '';
		}

		const [firstDigit] = cardNumber;
		allowedInitialDigits.forEach((allowedDigit) => {
			if (!isValid && firstDigit === allowedDigit) {
				isValid = true;
			}
		});

		return isValid ? '' : this.cardNumberErrorText;
	};

	getExpiresOnError = (expiresOn) => {
		if (!expiresOn) {
			return '';
		}

		return moment(expiresOn).isAfter(moment.now()) ? '' : this.expiresOnErrorText;
	};

	onFormSubmit = (event) => {
		const {
			id,
			cardNumber,
			userName,
			expiresOn,
		} = this.state;
		const { onSubmit } = this.props;

		event.preventDefault();

		onSubmit({
			id,
			cardNumber,
			userName,
			expiresOn,
		});
	};

	onUserNameChange = (event) => {
		const { value } = event.target;
		this.setState({ userName: value });
	};

	onCardNumberChange = (value) => {
		const { cardNumber } = this.state;

		const cardNumberError = this.getCardNumberError(value);

		this.setState({
			cardNumber: !cardNumberError ? value : cardNumber,
			cardNumberError,
		});
	};

	onExpiresOnChange = (event) => {
		const {
			expiresOn,
		} = this.state;

		const { value } = event.target;

		const expiresOnError = this.getExpiresOnError(value);

		this.setState({
			// If no error for new value and new value exists (didnt press x on date picker), set value else leave the one it has
			expiresOn: !expiresOnError && value ? value : expiresOn,
			// If value exists (no reset) then set expiresOnError (either '' or real error), else get error on date that is on card - expiresOn
			// (expiresOn can be empty if adding a new card, so just recalculate)
			expiresOnError: value ? expiresOnError : this.getExpiresOnError(expiresOn),
		});
	};

	render() {
		const {
			cardNumber,
			userName,
			expiresOn,
			cardNumberError,
			expiresOnError,
		} = this.state;

		return (
			<CardFormWrapper>
				<Card
					cardNumber={cardNumber}
					userName={userName}
					expiresOn={expiresOn}
				/>
				<BaseDivider />
				<form onSubmit={this.onFormSubmit}>
					<BaseInput
						label="Name"
						type="text"
						onChange={this.onUserNameChange}
						defaultValue={userName}
					/>
					<CardNumberInput
						defaultValue={cardNumber}
						onChange={this.onCardNumberChange}
						error={cardNumberError}
					/>
					<BaseInput
						label="Expires on"
						type="date"
						onChange={this.onExpiresOnChange}
						defaultValue={expiresOn}
						error={expiresOnError}
					/>
					<PaddingWrapper top={20}>
						<BaseButton disabled={!this.isFormValid(userName, cardNumber, expiresOn, cardNumberError, expiresOnError)}>
							SAVE
						</BaseButton>
					</PaddingWrapper>
				</form>
			</CardFormWrapper>
		);
	}
}

CardForm.propTypes = {
	initialState: PropTypes.shape({
		id: PropTypes.string,
		cardNumber: PropTypes.string,
		userName: PropTypes.string,
		expiresOn: PropTypes.string,
	}),
	onSubmit: PropTypes.func.isRequired,
};

CardForm.defaultProps = {
	initialState: {
		id: '',
		cardNumber: '',
		userName: '',
		expiresOn: '',
	},
};

export default CardForm;
