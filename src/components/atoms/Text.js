import styled from 'styled-components';

import { Colors, FontSizes } from '../../lib/constants/Style';

export const PageHeading = styled.h1`
	${FontSizes.pageHeading};
	margin: 20px 0;
	text-align: center;
	font-weight: bold;
`;

export const ErrorText = styled.p`
	${FontSizes.base};
	${Colors.baseRed};
	padding: 5px 0;
	height: 26px;
`;
