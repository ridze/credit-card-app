import styled from 'styled-components';
import { Link } from 'react-router-dom';
// Atoms
import { FlexColumnAlignCenter } from './Flex';

export const CardFormWrapper = styled(FlexColumnAlignCenter)`
	width: 400px;
	form {
		width: 100%
	}
`;

export const PaddingWrapper = styled.div`
	width: ${props => (props.width ? `${props.width}px` : '100%')};
	width: ${props => (props.widthPercentage ? `${props.widthPercentage}%` : '100%')};
	height: ${props => (props.height ? `${props.height}px` : 'auto')};
	padding: ${props => `${props.top || 0}px ${props.right || 0}px ${props.bottom || 0}px ${props.left || 0}px`};
`;

export const CardLinkWrapper = styled(Link)`
	margin: 20px;
	cursor: pointer;
	text-decoration: none;
`;
