import styled from 'styled-components';

export const InputComponent = styled.input`
	width: 100%;
	padding: 10px;
`;
