import styled from 'styled-components';

// Css
import { Colors, BackgroundColors } from '../../lib/constants/Style';

export const BaseButton = styled.button`
	${BackgroundColors.baseBlue};
	${Colors.baseWhite};
	width: 100%;
	border: none;
	padding: 10px 0;
	cursor: pointer;

	&&:disabled {
		${BackgroundColors.transparentBlue};
	}
`;
