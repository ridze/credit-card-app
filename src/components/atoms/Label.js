import styled from 'styled-components';

export const LabelComponent = styled.label``;

export const LabelWrapper = styled.div`
	padding: 10px 0;
`;
