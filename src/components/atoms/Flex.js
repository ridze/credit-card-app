import styled from 'styled-components';

export const FlexRow = styled.div`
	display: flex;
	align-items: center;
	flex-wrap: wrap;
	width: 100%;
`;

export const FlexRowSpaceBetween = styled(FlexRow)`
	justify-content: space-between;
`;

export const FlexRowSpaceAround = styled(FlexRow)`
	justify-content: space-around;
`;

export const FlexColumn = styled.div`
	display: flex;
	flex-direction: column;
`;

export const FlexColumnAlignCenter = styled(FlexColumn)`
	align-items: center;
`;
