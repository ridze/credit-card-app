import styled from 'styled-components';

// Style
import { FontSizes } from '../../lib/constants/Style';

// Images
import CardImage from '../../assets/images/card.png';
import CardChipImage from '../../assets/images/card-chip.png';

// Atoms
import { FlexColumn, FlexRowSpaceAround } from './Flex';

export const CardWrapper = styled(FlexColumn)`
	position: relative;
	border-radius: 15px;
	box-sizing: border-box;
	padding: 10px 20px 30px 20px;
	width: 400px;
	height: 250px;
	background: url(${CardImage}) no-repeat fixed center;
	background-size: 100% 100%;
`;

export const CardChip = styled.div`
	width: 50px;
	min-height: 44px;
	height: 44px;
	background: url(${CardChipImage}) no-repeat;
	background-size: cover;
	align-self: flex-start;
`;

export const CardTypeWrapper = styled(FlexRowSpaceAround)`
	width: 150px;
	height: 50px;
	margin-left: auto;
`;

export const CardImageVisa = styled.img`
	width: 140px;
    height: 130px;
    margin-top: -40px;
`;

export const CardImageMaster = styled.img`
	width: 90px;
    height: 53px;
`;

export const CardImageDiscover = styled.img`
	width: 100px;
    height: 17px;
`;

export const CardNumber = styled.div`
	${FontSizes.huge};
	height: 24px;
	margin-top: 25px;
	margin-bottom: 25px;
	font-weight: 400;
`;

export const UserName = styled.div`
	${FontSizes.big};
	width: 300px;
	text-overflow: ellipsis;
	overflow: hidden;
	height: 20px;
	font-weight: 400;
	text-transform: uppercase;
`;

export const ExpiresOn = styled.div`
	${FontSizes.big};
	height: 20px;
	font-weight: 400;
	text-transform: uppercase;
`;

export const PlusSign = styled.span`
	position: absolute;
	left: 50%;
	top: calc(50% - 10px);
	transform: translate(-50%, -50%);
	font-size: 120px;
	line-height: 1;
`;
