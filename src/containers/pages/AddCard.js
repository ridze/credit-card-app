import React from 'react';
import PropTypes from 'prop-types';
import uuid from 'uuid';

// Lib
import LocalStorage from '../../lib/LocalStorage';

// Atoms
import { PageHeading } from '../../components/atoms/Text';
import { FlexColumnAlignCenter } from '../../components/atoms/Flex';

// Organisms
import CardForm from '../../components/organisms/CardForm';

const AddCard = (props) => {
	const onAddCard = (cardData) => {
		const {
			history,
		} = props;

		try {
			cardData.id = uuid();
			const cards = LocalStorage.getItem('cards');
			cards.push(cardData);
			LocalStorage.setItem('cards', cards);
			history.push('/cards');
		} catch (error) {
			console.log(error);
		}
	};

	return (
		<FlexColumnAlignCenter>
			<PageHeading>Add card to account</PageHeading>
			<CardForm onSubmit={onAddCard} />
		</FlexColumnAlignCenter>
	);
};

AddCard.propTypes = {
	history: PropTypes.shape({
		push: PropTypes.func.isRequired,
	}).isRequired,
};

export default AddCard;
