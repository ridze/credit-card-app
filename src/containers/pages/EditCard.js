import React from 'react';
import PropTypes from 'prop-types';

// Lib
import LocalStorage from '../../lib/LocalStorage';

// Atoms
import { PageHeading } from '../../components/atoms/Text';
import { FlexColumnAlignCenter } from '../../components/atoms/Flex';

// Organisms
import CardForm from '../../components/organisms/CardForm';

const AddCard = (props) => {
	const {
		match,
		history,
	} = props;

	const { id: cardId } = match.params;

	const onUpdateCard = (cardData) => {
		try {
			const cards = LocalStorage.getItem('cards');
			const cardIndex = cards.findIndex(card => card.id === cardId);
			cards[cardIndex] = cardData;
			LocalStorage.setItem('cards', cards);
			history.push('/cards');
		} catch (error) {
			console.log(error);
		}
	};

	const getCardToEdit = () => {
		const cards = LocalStorage.getItem('cards');
		const cardToEdit = cards.find(card => card.id === cardId);
		if (cardToEdit) {
			return cardToEdit;
		}
		alert('Card does not exist');
		history.push('/cards');
		return false;
	};

	return (
		<FlexColumnAlignCenter>
			<PageHeading>Edit current card</PageHeading>
			<CardForm initialState={getCardToEdit()} onSubmit={onUpdateCard} />
		</FlexColumnAlignCenter>
	);
};

AddCard.propTypes = {
	match: PropTypes.shape({
		params: PropTypes.shape({
			id: PropTypes.string.isRequired,
		}).isRequired,
	}).isRequired,
	history: PropTypes.shape({
		push: PropTypes.func.isRequired,
	}).isRequired,
};

export default AddCard;
