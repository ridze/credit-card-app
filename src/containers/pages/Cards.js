import React, { PureComponent } from 'react';

// Lib
import LocalStorage from '../../lib/LocalStorage';

// Atoms
import { PageHeading } from '../../components/atoms/Text';
import { FlexColumnAlignCenter, FlexRow } from '../../components/atoms/Flex';
import { CardLinkWrapper } from '../../components/atoms/Wrapper';

// Molecules
import Card from '../../components/molecules/Card';
import AddNewCard from '../../components/molecules/CardAddNew';

class Cards extends PureComponent {
	constructor(props) {
		super(props);
		this.cards = LocalStorage.getItem('cards') || [];
	}

	render() {
		return (
			<FlexColumnAlignCenter>
				<PageHeading>My cards</PageHeading>
				<FlexRow>
					{this.cards.map((card) => {
						const {
							id,
							cardNumber,
							userName,
							expiresOn,
						} = card;

						return (
							<CardLinkWrapper key={id} to={`/cards/${id}/edit`}>
								<Card
									cardNumber={cardNumber}
									userName={userName}
									expiresOn={expiresOn}
								/>
							</CardLinkWrapper>
						);
					})}
					<CardLinkWrapper to="/cards/add">
						<AddNewCard />
					</CardLinkWrapper>
				</FlexRow>
			</FlexColumnAlignCenter>
		);
	}
}

export default Cards;
