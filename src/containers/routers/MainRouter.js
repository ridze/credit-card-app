import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

// Pages
import AddCard from '../pages/AddCard';
import EditCard from '../pages/EditCard';
import Cards from '../pages/Cards';

const MainRouter = () => (
	<BrowserRouter>
		<Switch>
			<Route
				exact
				path="/cards"
				component={Cards}
			/>
			<Route
				exact
				path="/cards/add"
				component={AddCard}
			/>
			<Route
				exact
				path="/cards/:id/edit"
				component={EditCard}
			/>
		</Switch>
	</BrowserRouter>
);

export default MainRouter;
