import React, { Fragment } from 'react';

// Css
import GlobalStyle from '../css/Global';

// Routers
import MainRouter from './routers/MainRouter';

// Set some initial data to work with
require('../lib/SetInitialData');

const Main = () => (
	<Fragment>
		<GlobalStyle />
		<div id="CreditCardAppTest">
			<MainRouter />
		</div>
	</Fragment>
);

export default Main;
